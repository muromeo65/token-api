Rails.application.routes.draw do
  mount GraphiQL::Rails::Engine, at: "/graphiql", graphql_path: "api/v1/graphql" if Rails.env.development?

  namespace :api do
    namespace :v1 do
      post "/graphql", to: "graphql#execute"

      # Authentication
      post 'login',      to: 'authentications#authenticate'
      post 'sign_up',    to: 'authentications#create'

      # User
      post 'users/delete',     to: 'users#delete'
      post 'users/edit',       to: 'users#edit'

    end
  end
end
