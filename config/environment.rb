# Load the Rails application.
require_relative 'application'

# Initialize the Rails application.
Rails.application.initialize!

# Recursively requires all files in ./app and down that end in .rb
Dir.glob('./app/*').each do |folder|
  Dir.glob("#{folder}/*.rb").each do |file|
    require file
  end
end
