FactoryBot.define do
  factory :user, class: User do
    name                    { Faker::Artist.name }
    email                   { Faker::Internet.email }
    password                { 'qwerty' }
    password_confirmation   { 'qwerty' }
    admin                   { false }
  end
end
