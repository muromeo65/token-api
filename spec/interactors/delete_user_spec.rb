require 'rails_helper'

RSpec.describe DeleteUser, type: :interactor do
  describe '.call' do
    context 'Quando for deletar um candidato' do

      context 'e o user_id estiver nulo' do
        it 'deve retornar um erro' do
          result = described_class.call(user_id: nil)

          expect(result).to be_a_failure
          expect(result.context.user_id).to be nil
        end
      end

      context 'e o user não existir' do
        it 'deve retornar um erro' do
          result = described_class.call(user_id: -1)

          expect(result).to be_a_failure
          expect(result.error).to eq("Usuário não existe")
        end
      end

      context 'e o user existir' do
        let!(:user) { create(:user) }

        it 'deve ser deletado com sucesso' do
          result = described_class.call(user_id: user.id)

          expect(result).to be_a_success
          expect(User.where(id: user.id).first).to be nil
          expect(result.deleted_user.email).to eq(user.email)
        end
      end
    end
  end
end
