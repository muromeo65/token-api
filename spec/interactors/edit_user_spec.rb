require 'rails_helper'

RSpec.describe EditUser, type: :interactor do
  describe '.call' do

    context 'Quando user_id estiver nulo' do
      it 'deve retornar um erro' do
        result = described_class.call(user_id: nil)

        expect(result).to be_a_failure
        expect(result.context.user_id).to be nil
      end
    end

    context 'Quando user não existir' do
      it 'deve retornar um erro' do
        result = described_class.call(user_id: -1)

        expect(result).to be_a_failure
      end
    end

    context 'Quando o user existe' do
      let!(:user) { create(:user) }

      context 'e passo os parametros errados' do
        it 'deverá retornar erro' do
          result = described_class.call(
            user_id: user.id,
            new_email: nil,
            new_password: nil,
            new_name: nil
          )

          expect(result).to be_a_failure
          expect(result.context.new_name).to be nil
          expect(result.context.new_email).to be nil
          expect(result.context.new_password).to be nil
        end
      end

      context 'e passo o email novo' do
        it 'deverá ser editado com sucesso' do
          result = described_class.call(
            user_id: user.id,
            new_email: 'email_novo@gmail.com'
          )

          expect(result).to be_a_success
          expect(result.edited_user.email).to eq 'email_novo@gmail.com'
        end
      end

      context 'e passo a senha nova' do
        it 'deverá ser editado com sucesso' do
          result = described_class.call(
            user_id: user.id,
            new_password: 'senha_123'
          )

          expect(result).to be_a_success
          expect(result.edited_user.password).to eq 'senha_123'
        end
      end

      context 'e passo senha e email novos' do
        it 'deverá ser ditado com sucesso' do
          result = described_class.call(
            user_id: user.id,
            new_email: 'email_novo2@gmail.com',
            new_password: 'senha_666',
            new_name: 'João Passos'
          )

          expect(result).to be_a_success
          expect(result.edited_user.email).to eq 'email_novo2@gmail.com'
          expect(result.edited_user.password).to eq 'senha_666'
          expect(result.edited_user.name).to eq 'João Passos'
        end
      end

      context 'e passo o nome novo' do
        it 'deverá ser editado com sucesso' do
          result = described_class.call(
            user_id: user.id,
            new_name: 'Cláudio Lemos'
          )

          expect(result).to be_a_success
          expect(result.edited_user.name).to eq 'Cláudio Lemos'
        end
      end

    end
  end
end
