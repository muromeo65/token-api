# Token Api

A JSON token based api with authentication preconfigured.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development.

### Prerequisites

Things you need to install the project.

```
Postgres 	>= 9.6.12
Ruby 		>= 2.5.1
Rails		>= 5.2.3
```

### Installing

How to setup the environment:

1 - Clone the project

```
git clone https://LeQuack@bitbucket.org/LeQuack/token-api.git
```

2 - Change config/database.yml to your postgres username and password

```
default: &default
  adapter: postgresql
  encoding: unicode
  database: your_database_name
  username: yout_database_username
  pool: <%= ENV.fetch("RAILS_MAX_THREADS") { 5 } %>

development:
  <<: *default
  host: localhost
  password: your_databse_password
```

3 - Setup database

```
rake db:setup
```

4 - Run the project

```
rails s
```

Good to go!

## How to use

### Creating a new user

With PostMan or some restful request program, send a `JSON` POST request to  `http://localhost:3000/api/v1/sign_up/` with fields like:

```
{
	"name": "Coop",
	"email": "tars@endurance.com",
	"password": "gravity",
	"password_confirmation": "gravity",
	"admin": true,
  	"locale": "en"
}
```
Default locale is `en`, so if you don't pass locale, your user's native language will be `english`

Only english `'en'` and brazilian portuguese `'pt_BR'` are available

### Login
Send a `JSON` POST request to `http://localhost:3000/api/v1/login/` with your email and password:

```
{
	"email": "tars@endurance.com",
	"password": "gravity"
}
```

Then, a token will be generated as response:

```
{
	"auth_token": "eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJleHAiOjE1NzQ0NzE0MzV9.RPOt01FcfER6tTbClsHq5GTKbx3HWfOAYHy9YV4YmlQ"
}
```

Just use add in your header and you be authenticated as your own user!

```
{
	"Content-Type": "application/json",
	"Authentication": "eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJleHAiOjE1NzQ0NzE0MzV9.RPOt01FcfER6tTbClsHq5GTKbx3HWfOAYHy9YV4YmlQ"
}
```

## Author

**Murilo Romeo** - *Ruby on Rails developer* - [LeQuack]([https://bitbucket.org/LeQuack])
