class EditUser
  include Interactor

  def call
    raise 'User_id não pode estar em branco'        if context.user_id.blank?
    raise 'Novas informações não podem ser nulas'   if validation_error

    user = User.where(id: context.user_id).first
    raise 'Usuário não encontrado' if user.blank?

    user.name       = context.new_name      if context.new_name.present?
    user.email      = context.new_email     if context.new_email.present?
    user.password   = context.new_password  if context.new_password.present?

    user.save
    context.edited_user = user
  rescue => e
    context.fail!(error: e.message, context: context)
  end

  private

  def validation_error
    context.new_email.blank?      &&
    context.new_password.blank?   &&
    context.new_name.blank?
  end
end
