class DeleteUser
  include Interactor

  def call
    raise 'User_id não pode estar em branco' if context.user_id.blank?

    user = User.where(id: context.user_id).first
    raise 'Usuário não existe' if user.blank?

    user.delete
    context.deleted_user = user
  rescue => e
    context.fail!(error: e.message, context: context)
  end
end
