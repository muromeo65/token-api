class ApplicationController < ActionController::API
  before_action :authenticate_request
  attr_reader :current_user

  def is_admin?
    render json: { error: 'Você não tem permissão!' }, status: 401 unless @current_user.admin
  end

  private

  def authenticate_request
    @current_user = AuthorizeApiRequest.call(request.headers).result
    render json: { error: 'Você não tem autorização!' }, status: 401 unless @current_user
  end

  def parse_response
    # return unless
    # response_body = JSON.parse(response_body.first).dig('data').map { |r| r[1] }.first
  end

end
