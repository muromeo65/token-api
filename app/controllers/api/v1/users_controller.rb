class Api::V1::UsersController < ApplicationController
  before_action :set_params

  def delete
    result = DeleteUser.call(user_id: current_user.id)

    if result.success?
      render json: { success: "#{result.deleted_user.name} deletado com sucesso" }
    else
      render json: { error: result.error }
    end
  end

  def edit
    result = EditUser.call(
      user_id: current_user.id,
      new_name: @user.new_name,
      new_email: @user.new_email,
      new_password: @user.new_password,
    )

    if result.success?
      render json: { success: "#{result.edited_user.name} editado com sucesso" }
    else
      render json: { error: result.error }
    end
  end

  private

  def set_params
    @user = OpenStruct.new(user_params)
  end

  def user_params
    params.permit(
      :new_name,
      :new_email,
      :new_password
    )
  end
end
