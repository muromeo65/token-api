class Api::V1::AuthenticationsController < ApplicationController
  skip_before_action :authenticate_request

  def authenticate
    user = user_params
    command = AuthenticateUser.call(user[:email], user[:password])

    if command.success?
      render json: { auth_token: command.result }
    else
      render json: { error: command.errors }, status: :unauthorized
    end
  end

  def create
    user = User.new(user_params)

    if user.save
      render json: { success: "Usuário criado com sucesso" }
    else
      render json: user.errors
    end
  end

  private

  def user_params
    params.permit(
      :name,
      :email,
      :password,
      :password_confirmation
    )
  end

end
