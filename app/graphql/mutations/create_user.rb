module Mutations
  class CreateUser < Mutations::BaseMutation
    argument :name,                   String, required: true
    argument :email,                  String, required: true
    argument :password,               String, required: true
    argument :password_confirmation,  String, required: true
    argument :admin,                  String, required: false

    field :user,    Types::UserType, null: true
    field :status,  String, null: false

    def resolve(name:, email:, password:, password_confirmation:, admin:)
      user = User.new(
        name: name,
        email: email,
        password: password,
        password_confirmation: password_confirmation,
        admin: admin
      )

      if user.save
        { user: user, status: 'success' }
      else
        { user: user, status: 'fail' }
      end
    end
  end
end
