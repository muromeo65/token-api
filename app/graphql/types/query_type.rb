module Types
  class QueryType < Types::BaseObject
    field :all_users,
          [Types::UserType],
          null: false,
          description: 'Return all Users in database'

    field :my_user,
          Types::UserType,
          null: false,
          description: 'Return who is the current_user'

    def all_users
      User.all
    end

    def my_user
      context[:current_user]
    end

  end
end
