module Types
  class MutationType < Types::BaseObject
    field :login,     mutation: Mutations::Authentication
    field :sign_up,   mutation: Mutations::CreateUser
  end
end
